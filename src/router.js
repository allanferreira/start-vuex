import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from './components/Home/Home.vue'
import Blog from './components/Blog/Blog.vue'

Vue.use(VueRouter)

export default new VueRouter({
	mode: 'history',
	routes: [
  		{
			path: '/',
			component: Home
		},
		{
			path: '/Blog',
			component: Blog
  		}
	]
})
